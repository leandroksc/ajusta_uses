unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Menus, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls,
  System.ImageList, Vcl.ImgList;

type
  TFrmPrincipal = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    MemoOrigem: TMemo;
    MemoDestino: TMemo;
    Panel2: TPanel;
    BtnConfiguracoes: TButton;
    Splitter1: TSplitter;
    ImageList1: TImageList;
    procedure BtnConfiguracoesClick(Sender: TObject);
    procedure MemoDestinoChange(Sender: TObject);
    procedure MemoOrigemChange(Sender: TObject);
  private
    { Private declarations }
    procedure ProcessarUses;
  public
    { Public declarations }
  end;

const
  USES_INICIAIS: TArray<string> = ['System.', 'Windows.', 'Vcl.'];
  USES_FIXAS: TArray<string> = ['Dialogs', 'Windows', 'Classes', 'SysUtils', 'Forms', 'Controls', 'Graphics', 'Messages'];
  CHARS_LINHA = 120;

var
  FrmPrincipal: TFrmPrincipal;

implementation

{$R *.dfm}

{ TFrmPrincipal }

uses
  StrUtils, Configuracoes;

procedure TFrmPrincipal.BtnConfiguracoesClick(Sender: TObject);
begin
  TFrmConfiguracoes.Create(Self).ShowModal;
end;


procedure TFrmPrincipal.MemoDestinoChange(Sender: TObject);
begin
  StatusBar1.Panels[1].Text := 'Processado: ' + MemoDestino.Lines.Count.ToString;
end;


procedure TFrmPrincipal.MemoOrigemChange(Sender: TObject);
begin
  ProcessarUses;
  StatusBar1.Panels[0].Text := 'Original: ' + MemoOrigem.Lines.Count.ToString;
end;

function OrdenacaoUses(List: TStringList; Index1, Index2: Integer): Integer;
begin
  Result := Length(List[Index2]) - Length(List[Index1]);
end;

procedure TFrmPrincipal.ProcessarUses;
var
  AUses1, AUses2: TStringList;
  AArray: TArray<string>;
  i, j: integer;
  ALinha: string;
begin
  MemoDestino.Lines.Clear;
  AUses1 := TStringList.Create;
  AUses2 := TStringList.Create;
  try
    AArray := MemoOrigem.Lines.Text.Split([',']);
    for I := Low(AArray) to High(AArray) do
      AUses1.Add(ReplaceStr(Trim(AArray[i]), ';', ''));

    for I := AUses1.Count - 1 downto 0 do
    begin
      for J := Low(USES_INICIAIS) to High(USES_INICIAIS) do
      begin
        if StartsText(USES_INICIAIS[j], AUses1[i]) then
        begin
          AUses2.Add(AUses1[i]);
          AUses1.Delete(i);
        end;
      end;

      for J := Low(USES_FIXAS) to High(USES_FIXAS) do
      begin
        if SameText(USES_FIXAS[j], AUses1[i]) then
        begin
          AUses2.Add(AUses1[i]);
          AUses1.Delete(i);
        end;
      end;
    end;

    AUses2.CustomSort(OrdenacaoUses);
    AUses1.CustomSort(OrdenacaoUses);

    while (AUses1.Count > 0) or (AUses2.Count > 0) do
    begin
      I := 0;
      while I < AUses2.Count do
      begin
        if Length(ALinha + AUses2[i]) + 2 < CHARS_LINHA then
        begin
          if ALinha <> '' then
            ALinha := ALinha + ', ';
          ALinha := ALinha + AUses2[i];
          AUses2.Delete(i);
        end else
          Inc(I);
      end;

      I := 0;
      while I < AUses1.Count do
      begin
        if Length(ALinha + AUses1[i]) + 2 < CHARS_LINHA then
        begin
          if ALinha <> '' then
            ALinha := ALinha + ', ';
          ALinha := ALinha + AUses1[i];
          AUses1.Delete(i);
        end else
          Inc(I);
      end;

      MemoDestino.Lines.Add(ALinha + IfThen((AUses1.Count > 0) or (AUses2.Count > 0), ',', ';'));
      ALinha := '';
    end;
  finally
    AUses1.Free;
    AUses2.Free;
  end;
end;

end.
