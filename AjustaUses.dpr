program AjustaUses;

uses
  Vcl.Forms,
  Principal in 'Principal.pas' {FrmPrincipal},
  Configuracoes in 'Configuracoes.pas' {FrmConfiguracoes};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.Run;
end.
