unit Configuracoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  System.ImageList, Vcl.ImgList;

type
  TFrmConfiguracoes = class(TForm)
    Panel1: TPanel;
    BtnAdd: TButton;
    BtnRemover: TButton;
    Panel2: TPanel;
    BtnConfirmar: TButton;
    BtnCancelar: TButton;
    Panel3: TPanel;
    Label1: TLabel;
    LstUses: TListBox;
    ImageList1: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure BtnConfirmarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FIniFile: string;
    procedure CarregarConfiguracoes;
    procedure SalvarConfiguracoes;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  IniFiles;

{ TFrmConfiguracoes }

procedure TFrmConfiguracoes.BtnConfirmarClick(Sender: TObject);
begin
  SalvarConfiguracoes;
end;

procedure TFrmConfiguracoes.CarregarConfiguracoes;
var
  AIni: TIniFile;
  ALista: TArray<string>;
  I: Integer;
begin
  AIni := TIniFile.Create(FIniFile);
  LstUses.Items.BeginUpdate;
  try
    LstUses.Items.Clear;
    ALista := AIni.ReadString('Config', 'UsesInicio', '').Split([',']);
    for I := Low(ALista) to High(ALista) do
      LstUses.Items.Add(ALista[i]);
  finally
    AIni.Free;
    LstUses.Items.EndUpdate;
  end;
end;

procedure TFrmConfiguracoes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TFrmConfiguracoes.FormCreate(Sender: TObject);
begin
  FIniFile := IncludeTrailingPathDelimiter(ExtractFilePath(Application.ExeName)) + 'Config.ini';
  CarregarConfiguracoes;
end;

procedure TFrmConfiguracoes.SalvarConfiguracoes;
var
  AIni: TIniFile;
  ALista: string;
  I: Integer;
begin
  AIni := TIniFile.Create(FIniFile);
  try
    for I := 0 to LstUses.Items.Count - 1 do
    begin
      if ALista <> '' then
        ALista := ALista + ',';
      ALista := ALista + LstUses.Items[i];
    end;
    AIni.WriteString('Config', 'UsesInicio', ALista);
  finally
    AIni.Free;
  end;
end;

end.
